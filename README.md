# README #

### What is this repository for? ###

The intention of this project is to develop a framework around which applications using the Nordic Semiconductor NRF24L01+ module can be easily built.
The focus is on modularity and portability.
While a skeleton implementation is not yet here, there are two demo applications which show how to set up a receiver and a transmitter using the Atmega328P and the Atmega32u4.
The code is very easily adapter to work with other micro-controllers in the AVR MEGA range as well as ported to other micro-controller families entirely with some basic knowledge of their operation.

### How do I get set up? ###

The demo applications are set up to open with Atmel Studio 6 or greater.

In future, there will be a skeleton framework with a makefile, compatible with any operating system with the necessary compilers for AVR.

### Contribution guidelines ###

If you wish to contribute or have any suggestions, requests or constructive criticism, feel free to make a pull request, email me or chat to me. I'm known as 'megal0maniac' on Freenode if IRC is your thing.

### Who do I talk to? ###

Michael Rodger (megal0maniac)

bitbucket [at] atinyhedgehog [dot] za [dot] net