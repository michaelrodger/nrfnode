/*
  Michael Rodger 23 August 2014
  NRF24L01+ command header file
  All taken from datasheet (https://www.nordicsemi.com/eng/nordic/download_resource/8765/2/8325448)
 */

//////////////////////////////////////////////////////////////////////////
////////////////////////////LEAVE THIS ALONE//////////////////////////////
//////////////////////////////////////////////////////////////////////////
#define R_REGISTER 0b00000000 //000A AAAA - Read command and status registers. A: 5bit register map address
#define W_REGISTER 0b00100000 //001A AAAA - Write command and status registers. A: 5bit register map address
#define R_RX_PAYLOAD 0b01100001 //Read RX payload: 1-32 bytes. A read operation always starts at byte 0. Payload is deleted from FIFO after it is read
#define W_TX_PAYLOAD 0b10100000 //Write TX payload: 1-32 bytes. A write operation always starts at byte 0 used in TX payload
#define FLUSH_TX 0b11100001 //Flush TX FIFO, used in TX mode
#define FLUSH_RX 0b11100010 //Flush RX FIFO, used in RX mode. Should not be executed during transmission of ACK. i.e. ACK will not be completed
#define REUSE_TX_PL 0b11100011 //Used for a PTX device. Reuse last transmitted payload. TX payload reuse is still active until W_TX_PAYLOAD or FLUSH_TX is executed. TX payload reuse must not be toggled during package transmission
#define R_RX_PL_WID 0b01100000 //Read RX payload width for the top R_RX_PAYLOAD in the RX FIFO (Note: Flush RX FIFO if the read value is larger than 32 bytes)
#define W_ACK_PAYLOAD 0b10101000 //1010 1PPP - Used in RX mode. Write payload to be transmitted together with ACK packet on pipe PPP. Maximum 3 ACK packet payloads can be pending. Payloads with same PPP are handled using FIFO principle. Write payload: 1-32 bytes. A write operation always starts at byte 0
#define W_TX_PAYLOAD_NO_ACK 0b10110000 //Used in TX mode. Disables AUTOACK on this specific packet
#define NOP 0b11111111 //No operation. Might be used to read the STATUS register

#define CONFIG 0x00 //Configuration register
#define MASK_RX_DR_bp 6 //Mask interrupt caused by RX_DR. 1: Interrupt not reflected on the IRQ pin. 0: Reflect RX_DR as active low interrupt on the IRQ pin
#define MASK_TX_DS_bp 5 //Mask interrupt caused by TX_DS. 1: Interrupt not reflected on the IRQ pin. 0: Reflect TX_DS as active low interrupt on the IRQ pin
#define MASK_MAX_RT_bp 4 //Mask interrupt caused by MAX_RT. 1: Interrupt not reflected on the IRQ pin. 0: Reflect MAX_RT as active low interrupt on the IRQ pin
#define EN_CRC_bp 3 //Enable CRC. Forced high if one of the bits in the EN_AA is high
#define CRCO_bp 2 //CRC encoding scheme. 0: 1 byte. 1: 2 bytes
#define PWR_UP_bp 1 // 1: Power up. 0: Power down
#define PRIM_RX 0 //1: PRX. 0: PTX

#define EN_AA 0x01 //Enhanced Shockburst. Disable 'auto acknowledgment for compatibility with nRF24L01
#define ENAA_P5_bp 5 //Enable auto acknowledgment data pipe 5
#define ENAA_P4_bp 4 //Enable auto acknowledgment data pipe 4
#define ENAA_P3_bp 3 //Enable auto acknowledgment data pipe 3
#define ENAA_P2_bp 2 //Enable auto acknowledgment data pipe 2
#define ENAA_P1_bp 1 //Enable auto acknowledgment data pipe 1
#define ENAA_P0_bp 0 //Enable auto acknowledgment data pipe 0

#define EN_RXADDR 0x02 //Enabled RX addresses
#define ERX_P5_bp 5 //Enable data pipe 5
#define ERX_P4_bp 4 //Enable data pipe 4
#define ERX_P3_bp 3 //Enable data pipe 3
#define ERX_P2_bp 2 //Enable data pipe 2
#define ERX_P1_bp 1 //Enable data pipe 1
#define ERX_P0_bp 0 //Enable data pipe 0

#define SETUP_AW 0x03 //Setup of address widths (common for all data pipes)
#define AW_bm 0b00000011
#define AW_3BYTE 0b01
#define AW_4BYTE 0b10
#define AW_5BYTE 0b11

#define SETUP_RETR 0x04 //Setup of automatic retransmission
#define ARD_bm 0xf0 //Automatic retransmission delay (Defined from end of transmission to start of next transmission)
#define ARD_250US 0b0000
#define ARD_500US 0b0001
#define ARD_750US 0b00100
#define ARD_1000US 0b0011
#define ARD_1250US 0b0100
#define ARD_1500US 0b0101
#define ARD_1750US 0b0110
#define ARD_2000US 0b0111
#define ARD_2250US 0b1000
#define ARD_2500US 0b1001
#define ARD_2750US 0b1010
#define ARD_3000US 0b1011
#define ARD_3250US 0b1100
#define ARD_3500US 0b1101
#define ARD_3750US 0b1110
#define ARD_4000US 0b1111

#define ARC_bm 0x0f //Automatic retransmission count
#define ARC_DISABLED 0
#define ARC_1 1
#define ARC_2 2
#define ARC_3 3
#define ARC_4 4
#define ARC_5 5
#define ARC_6 6
#define ARC_7 7
#define ARC_8 8
#define ARC_9 9
#define ARC_10 10
#define ARC_11 11
#define ARC_12 12
#define ARC_13 13
#define ARC_14 14
#define ARC_15 15

#define RF_CH 0x05 //RF channel
#define RF_CH_mb 0b01111111

#define RF_SETUP 0x06 //RF setup register
#define CONT_WAVE_bp 7 //Enables continuous carrier transmit when high
#define RF_DR_LOW_bp 5 //Set RF data rate to 250kbps
#define PLL_LOCK_bp 4 //Force PLL lock signal. Only used in test
#define RF_DR_HIGH_bp 3 //Select between the high speed data rates. 0: 1Mbps. 1: 2Mbps
#define RF_PWR_bp 0b110 //RF output power in TX mode
#define RF_PWR_M18 0b00
#define RF_PWR_M12 0b01
#define RF_PWR_M6 0b10
#define RF_PWR_0 0b11

#define STATUS 0x07 //Status register (in parallel to the SPI command word applied on the MOSI pin, the STATUS register is shifted serially out on the MISO pin)
#define RX_DR_bp 6 //Data ready RX FIFO interrupt. Asserted when new data arrives RX FIFO. Write 1 to clear
#define TX_DS_bp 5 //Data sent TX FIFO interrupt. Asserted when packet transmitted on TX. If AUTO_ACK is activated, this bit is set high only when ACK is received. Write 1 to clear
#define MAX_RT_bp 4 //Maximum number of TX transmits interrupt. Write 1 to clear. If MAX_RT is asserted it must be cleared to enable further communication
#define RX_P_NO_bm 0b1110 //Data pipe number for the payload available for reading from RX_FIFO
#define RX_P_NO_DP0 0
#define RX_P_NO_DP1 1
#define RX_P_NO_DP2 2
#define RX_P_NO_DP3 3
#define RX_P_NO_DP4 4
#define RX_P_NO_DP5 5
//#define TX_FULL_bp 0 //TX FIFO full flag. 1: TX FIFO full. 0: Available locations in TX FIFO. Please use the bit in the FIFO_STATUS register

#define OBSERVE_TX 0x08 //Transmit observe register
#define PLOS_CNT_bm 0xf0 //Count lost packets. The counter is overflow protected to 15 and discontinues at max until reset. The counter is reset by writing to RF_CH
#define ARC_CNT_bm 0x0f //Count retransmitted packets. The counter is reset when transmission of a new packet starts

#define RPD 0x09
#define RPD_bp 0 //Received power detector

#define RX_ADDR_P0 0x0a
#define RX_ADDR_P1 0x0b
#define RX_ADDR_P2 0x0c
#define RX_ADDR_P3 0x0d
#define RX_ADDR_P4 0x0e
#define RX_ADDR_P5 0x0f

#define TX_ADDR 0x10

#define RX_PW_P0 0x11
#define RX_PW_P0_bm 0x1F //Number of bytes in RX payload in data pipe 0 (1 to 32 bytes)
#define RX_PW_P1 0x12
#define RX_PW_P1_bm 0x1F //Number of bytes in RX payload in data pipe 1 (1 to 32 bytes)
#define RX_PW_P2 0x13
#define RX_PW_P2_bm 0x1F //Number of bytes in RX payload in data pipe 2 (1 to 32 bytes)
#define RX_PW_P3 0x14
#define RX_PW_P3_bm 0x1F //Number of bytes in RX payload in data pipe 3 (1 to 32 bytes)
#define RX_PW_P4 0x15
#define RX_PW_P4_bm 0x1F //Number of bytes in RX payload in data pipe 4 (1 to 32 bytes)
#define RX_PW_P5 0x16
#define RX_PW_P5_bm 0x1F //Number of bytes in RX payload in data pipe 5 (1 to 32 bytes)

#define FIFO_STATUS 0x17 //FIFO status register
#define TX_REUSE_bp 6 //Used for a PTX device. Pulse the rfce high for at least 10uS to reuse last transmitted payload. TX payload reuse is active until W_TX_PAYLOAD or FLUSH_TX is executed. TX_REUSE is set by the SPI REUSE_TX_PL and is reset by the SPI commands W_TX_PAYLOAD or FLUSH_TX
#define TX_FULL_bp 5 //TX FIFO full flag. 1: TX FIFO full. 0: Available locations in TX FIFO
#define TX_EMPTY_bp 4 //TX FIFO empty flag. 1: TX FIFO empty. 0: Data in TX FIFO
#define RX_FULL_bp 1 //TX FIFO full flag. 1: RX FIFO full. 0: Available locations in RX FIFO
#define RX_EMPTY_bp 0 //RX FIFO empty flag. 1: RX FIFO empty. 0: Data in RX FIFO

#define DYNPD 0x1c //Enable dynamic payload length
#define DPL_P5_bp 5 //Enable dynamic payload length data pipe 5 (Requires EN_DPL and ENAA_P5)
#define DPL_P4_bp 4 //Enable dynamic payload length data pipe 4 (Requires EN_DPL and ENAA_P4)
#define DPL_P3_bp 3 //Enable dynamic payload length data pipe 3 (Requires EN_DPL and ENAA_P3)
#define DPL_P2_bp 2 //Enable dynamic payload length data pipe 2 (Requires EN_DPL and ENAA_P2)
#define DPL_P1_bp 1 //Enable dynamic payload length data pipe 1 (Requires EN_DPL and ENAA_P1)
#define DPL_P0_bp 0 //Enable dynamic payload length data pipe 0 (Requires EN_DPL and ENAA_P0)

#define FEATURE 0x1d
#define EN_DPL_bp 2 //Enabled dynamic payload length
#define EN_ACK_PAY_bp 1 //Enables payload with ACK
#define EN_DYN_ACK_bp 0 //Enables the W_TX_PAYLOAD_NOACK command