/*
 * _328Node.c
 *
 * Created: 2014/09/12 09:03:51 PM
 *  Author: Michael
 */ 

#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "328Node.h"
#include <util/delay.h>
#include <avr/wdt.h>

uint8_t currStatus;
uint8_t receiveBuffer[33];
uint8_t receiveBufferLength = 0;
uint8_t receiveBuffer0[33];
uint8_t receiveBuffer1[33];
uint8_t p0pres = 0;
uint8_t p1pres = 0;
uint8_t packetSource = 0;

#include "util.c"
#include "nrf24l01p.h"
#include "nrf24l01p.c"
#include "setup.c"

//Build byte by byte and transmit packet over serial.
void transmitPacket()
{
	char text[STRINGBUFFER];
	sprintf(text,"S|%d|",(receiveBufferLength-2)/3);
	UARTPrintString(text);
	int i;
// 	if(p0pres)
// 	{
// 		
// 		for(i = 1; i < receiveBufferLength-1; i = i+3)
// 		{
// 			uint16_t temp = (receiveBuffer0[i+1] << 8)  | (receiveBuffer0[i+2]);
// 			sprintf(text,"%u:%u|",receiveBuffer0[i],temp);
// 			UARTPrintString(text);
// 		}
// 	}
// 	if(p1pres)
// 	{
// 		
// 		for(i = 1; i < receiveBufferLength-1; i = i+3)
// 		{
// 			uint16_t temp = (receiveBuffer1[i+1] << 8)  | (receiveBuffer1[i+2]);
// 			sprintf(text,"%u:%u|",receiveBuffer1[i],temp);
// 			UARTPrintString(text);
// 		}
// 	}
	for(i = 1; i < receiveBufferLength-1; i = i+3)
	{
		uint16_t temp = (receiveBuffer[i+1] << 8)  | (receiveBuffer[i+2]);
		sprintf(text,"%u:%u|",receiveBuffer[i],temp);
		UARTPrintString(text);
	}
	sprintf(text,"E\n");
	UARTPrintString(text);
}

//Copies data from the receiveBuffer to the receiveBuffer specific to the pipe from which the data came
void copyArray(int pipe)
{
	if (pipe == 0)
	{
		int i;
		for(i=0; i < sizeof(receiveBuffer); i++)
		{
			receiveBuffer0[i] = receiveBuffer[i];
		}
	}
	else if (pipe == 1)
	{
		int i;
		for(i=0; i < sizeof(receiveBuffer); i++)
		{
			receiveBuffer1[i] = receiveBuffer[i];
		}
	}
}

int main(void)
{
	setupUART();
	setupSPI();
	setupNRF();
	FLUSHTX();
	FLUSHRX();
	WriteByteToNRF(W_REGISTER,STATUS,(1 << RX_DR_bp) | (1 << TX_DS_bp) | (1 << MAX_RT_bp)); //Clear interrupt flags

	while(1)
	{
		CE_HIGH(); //Switch to RX mode
		while(READBIT(FIFO_STATUS,RX_EMPTY_bp)) _delay_us(10); //Poll until RX_EMPTY is 0 (data in FIFO)
		receiveBufferLength = WriteByteToNRF(0, R_RX_PL_WID, NOP); //Store payload width
		CE_LOW();
		FLUSHTX();
		packetSource = WriteByteToNRF(R_REGISTER,STATUS,NOP) & (1 << RX_P_NO_bm); //Read source of data packet
		WriteBytesToNRF(0,R_RX_PAYLOAD,NULL,receiveBufferLength); //Put received data in register
		//Copy array
// 		if(packetSource == 0)
// 		{
// 			copyArray(0);
// 			p0pres = 1;
// 		}
// 		else if(packetSource == 1)
// 		{
// 			copyArray(1);
// 			p1pres = 1;
// 		}
		FLUSHRX();
		transmitPacket();
	}
	
}
