/*
  Michael Rodger 30 October 2014
  Some basic setup routines for UART, SPI, ADC and interrupts
 */

void setupADC()
{
	ADMUX = (1 << REFS0) | (1 << MUX2);
	ADCSRA = (1 << ADEN) | (1 << ADATE);
	ADCSRA |= (1 << ADSC);
	_delay_us(10);
}

void setupSPI()
{
	SPI_DDR |= (1 << SPI_MOSI) | (1 << SPI_SS) | (1 << SPI_SCK);
	SPI_DDR &= ~(1 << SPI_MISO);
	SPCR |= (1 << SPIE) | (1 << SPE) | (1 << MSTR);
}

//Registers are different on atmega328p and atmega32u4, adding ifdefs tell the compiler what code to compile
#ifdef __AVR_ATmega328P__
void setupUART()
{
	UART_DDR |= (1 << UART_TX); //TX output
	UART_DDR &= ~(1 << UART_RX); //RX input
	UCSR0B |= (1 << RXCIE0) | (1 << TXCIE0) | (1 << RXEN0) | (1 << TXEN0);
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ02);
	UBRR0 = 12; //76800 baud
	
}
#endif

#ifdef __AVR_ATmega32U4__
void setupUART()
{
	UART_DDR |= (1 << UART_TX); //TX output
	UART_DDR &= ~(1 << UART_RX); //RX input
	UCSR1B |= (1 << RXEN1) | (1 << TXEN1); //(1 << RXCIE1) | (1 << TXCIE1) | 
	UCSR1C |= (1 << UCSZ11) | (1 << UCSZ12);
	UBRR1 = 51; //19200 baud
	
}
#endif

void setupIRQ()
{
	NRF_PORT |= (1 << NRF_IRQ); //Enable pull-up for IRQ pin
	PCICR |= (1 << PCIE0); //Enable pin change interrupt 0
	PCMSK0 |= (1 << PCINT1); //Mask
}