//#define PTX
#define PRX
#define ADDRESS "PTX1"

#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SPI_SCK 5
#define SPI_MISO 4
#define SPI_MOSI 3
#define SPI_SS 2

#define UART_PORT PORTB
#define UART_DDR DDRB
#define UART_TX 1
#define UART_RX 0

#define NRF_PORT PORTB
#define NRF_DDR DDRB
#define NRF_IRQ 1
#define NRF_CE 0

#define STRINGBUFFER 80 //Maximum length for a string, to avoid infinite loops from malformed data
#define SEND 1
#define RECEIVE 0
#define F_CPU 16000000UL

#define SS_LOW() SPI_PORT &= ~(1 << SPI_SS)
#define SS_HIGH() SPI_PORT |= (1 << SPI_SS)
#define CE_LOW() NRF_PORT &= ~(1 << NRF_CE)
#define CE_HIGH() NRF_PORT |= (1 << NRF_CE)
#define SENDPACKET(X) WriteBytesToNRF(0, W_TX_PAYLOAD, X, sizeof(X))
#define SENDBYTE(X) WriteByteToNRF(0, W_TX_PAYLOAD, X)
#define FLUSHTX() WriteByteToNRF(0, FLUSH_TX, NOP)
#define FLUSHRX() WriteByteToNRF(0, FLUSH_RX, NOP)
#define READBIT(X,Y) (WriteByteToNRF(R_REGISTER, X, NOP) & (1 << Y))
#define READREG(X) (WriteByteToNRF(R_REGISTER, X, NOP))
#define SENDPACKET(X) WriteBytesToNRF(0, W_TX_PAYLOAD, X, sizeof(X))
#define SENDBYTE(X) WriteByteToNRF(0, W_TX_PAYLOAD, X)

