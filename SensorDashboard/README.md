# README #

### What is this? ###

This folder contains a demo front-end to display sensor readings transmitted by the 328Node demo project over serial.

While this is written in Java, currently it only seems to work on Windows (even with librxtx installed on Linux)

The start.bat file in /dist will detect your computer's architecture and run the program with the appropriate driver selected.

You should only need an up-to-date JRE installed.