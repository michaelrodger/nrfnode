/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Michael
 */
public class Channel
{
    int type;
    int value;
    public Channel(String ch)
    {
        String tmp[] = ch.split(":");
        type = Integer.parseInt(tmp[0]);
        value = Integer.parseInt(tmp[1]);
    }
    
    public int getType()
    {
        return type;
    }
    
    public int getValue()
    {
        return value;
    }
}
