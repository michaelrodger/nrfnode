@echo off
IF %PROCESSOR_ARCHITECTURE%==AMD64 (
echo 64bit operating system detected. Using AMD64 libraries...
cd x64\
) ELSE IF %PROCESSOR_ARCHITECTURE%==x86 (
echo 32bit operating system detected. Using x86 libraries...
cd x86\
)
echo Starting application
start java -jar ..\SensorDashboard.jar