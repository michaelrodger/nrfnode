#define PTX
//#define PRX
#define ADDRESS "PTX1"

#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define SPI_SCK 1
#define SPI_MISO 3
#define SPI_MOSI 2
#define SPI_SS 6

#define UART_PORT PORTD
#define UART_DDR DDRD
#define UART_TX 3
#define UART_RX 2

#define NRF_PORT PORTD
#define NRF_DDR DDRD
#define NRF_IRQ 4
#define NRF_CE 7

#define STRINGBUFFER 80 //Maximum length for a string, to avoid infinite loops from malformed data
#define SEND 1
#define RECEIVE 0
#define F_CPU 16000000UL

#define SS_LOW() SPI_PORT &= ~(1 << SPI_SS)
#define SS_HIGH() SPI_PORT |= (1 << SPI_SS)
#define CE_LOW() NRF_PORT &= ~(1 << NRF_CE)
#define CE_HIGH() NRF_PORT |= (1 << NRF_CE)
#define SENDPACKET(X) WriteBytesToNRF(0, W_TX_PAYLOAD, X, sizeof(X))
#define SENDBYTE(X) WriteByteToNRF(0, W_TX_PAYLOAD, X)
#define FLUSHTX() WriteByteToNRF(0, FLUSH_TX, NOP)
#define FLUSHRX() WriteByteToNRF(0, FLUSH_RX, NOP)
#define READBIT(X,Y) (WriteByteToNRF(R_REGISTER, X, NOP) & (1 << Y))
#define READREG(X) (WriteByteToNRF(R_REGISTER, X, NOP))
#define SENDPACKET(X) WriteBytesToNRF(0, W_TX_PAYLOAD, X, sizeof(X))
#define SENDPACKETVAR(X,Y) WriteBytesToNRF(0, W_TX_PAYLOAD, X, Y)
#define SENDBYTE(X) WriteByteToNRF(0, W_TX_PAYLOAD, X)

