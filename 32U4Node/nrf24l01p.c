/*
  Michael Rodger 30 October 2014
  Library of functions for NRF24L01+ module
 */ 

/*
  Send or receive a single byte.
  rw can be either W_REGISTER for a write operation or R_REGISTER for a read.
  reg is the address of the register you wish to access.
  rw is ORd with the register address, data is always written and data is always returned
*/
uint8_t WriteByteToNRF(uint8_t rw, uint8_t reg, uint8_t data)
{
	uint8_t response;
	SS_LOW();
	currStatus = SPIRW(rw | reg);
	response = SPIRW(data);
	SS_HIGH();
	return response;
}


/*
  Send or receive a series of bytes.
  rw can be either W_REGISTER for a write operation or R_REGISTER for a read.
  reg is the address of the register you wish to access.
  If W_REGISTER is specified, a pointer to a uint8_t array is required as well as the length of the array.
  If R_REGISTER is specified, data is read into receiveBuffer and length is stored in receiveBufferLength.
*/
void WriteBytesToNRF(uint8_t rw, uint8_t reg, uint8_t *data, uint8_t byteno)
{
	//_delay_us(10);
	SS_LOW();
	_delay_us(10);
	currStatus = SPIRW(rw | reg);
	_delay_us(10);
	int i;
	if((rw == W_REGISTER) | (reg == W_TX_PAYLOAD) | (reg == W_ACK_PAYLOAD))
	{
		for (i=0; i < byteno; i++)
		{
			SPIRW(data[i]);
			//_delay_us(10);
		}
	}
	else
	{
		receiveBufferLength = byteno;
		for (i=0; i < byteno; i++)
		{
			receiveBuffer[i] = SPIRW(NOP);
			//_delay_us(10);
		}
	}
	SS_HIGH();
}

void setupNRF()
{
	//Configure IO
	NRF_DDR |= (1 << NRF_CE);
	NRF_DDR &= ~(1 << NRF_IRQ);
	
	//Configure module
	CE_LOW();
	_delay_ms(100); //Give module time to find itself
	WriteByteToNRF(W_REGISTER, STATUS, (1 << RX_DR_bp) | (1 << TX_DS_bp) | (1 << MAX_RT_bp)); //Clear interrupt flags
	WriteByteToNRF(W_REGISTER, SETUP_RETR, (ARD_1500US << 4) | (ARC_15)); //Automatic retransmission delay is 1.5mS and it will retry 15 times before failure
	WriteByteToNRF(W_REGISTER, RF_CH, 'M');
	WriteByteToNRF(W_REGISTER, RF_SETUP, (1 << RF_DR_LOW_bp) | (RF_PWR_0 << 1)); //250kbps @ 0dbM
	WriteByteToNRF(W_REGISTER, DYNPD, (1 << DPL_P0_bp) | (1 << DPL_P1_bp)); //Dynamic payload enabled on pipes 0 and 1
	WriteByteToNRF(W_REGISTER, FEATURE, (1 << EN_DPL_bp) | (1 << EN_ACK_PAY_bp)); //Dynamic payload and payload with ACK enabled globally
	uint8_t address[] = {ADDRESS};
	WriteBytesToNRF(W_REGISTER, RX_ADDR_P0, address, sizeof(address)); //Configure address for pipe 0
	#ifdef PTX
	WriteByteToNRF(W_REGISTER, CONFIG, (1 << EN_CRC_bp) | (1 << PWR_UP_bp)); //Enable CRC and switch module on
	WriteBytesToNRF(W_REGISTER, TX_ADDR, address, sizeof(address)); //Configure TX address (if node is a primary transmitter)
	#else
	WriteByteToNRF(W_REGISTER, CONFIG, (1 << EN_CRC_bp) | (1 << PWR_UP_bp) | (1 << PRIM_RX)); //Enable CRC, enable primary receiver and switch module on
	#endif
	WriteByteToNRF(0, FLUSH_RX, NOP);
	
	_delay_ms(100);
}