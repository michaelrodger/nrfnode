/*
  Michael Rodger 30 October 2014
  Main .c file for project
 */ 

#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "32U4Node.h"
#include <util/delay.h>

uint8_t currStatus;
uint8_t receiveBuffer[32];
uint8_t receiveBufferLength = 0;

#include "util.c"
#include "nrf24l01p.h"
#include "nrf24l01p.c"
#include "setup.c"

//Configures D0 as input with pullup in order to send its value
void setupButton()
{
	DDRD &= ~(1 << 0); //Set button pin as input
	PORTD |= (1 << 0); //Enable pull-up
}

/*
This function builds a packet using various input sources and places it in the TX FIFO buffer.
*/
void sendAll()
{
	static uint8_t transmitBuffer[32];
	//SENDPACKET(adcbuff);
	int count = 0;
	static int ch4 = 0;
	//2 counters as dummy sensor values. They're incremented/decremented on each transmission.
	static uint8_t dummy;
	static uint16_t bigdummy;
	transmitBuffer[count++] = 'A'; //Node ID
	transmitBuffer[count++] = 0; //Type 0 (boolean)
	transmitBuffer[count++] = 0; transmitBuffer[count++] = (PIND & (1 << PIND0)); //Filler byte and button value
	transmitBuffer[count++] = 2; //Type 2 (generic variable 10bit)
	transmitBuffer[count++] = ADCH; transmitBuffer[count++] = ADCL; //10bit ADC value
	transmitBuffer[count++] = 1; //Type 1 (generic 8bit)
	transmitBuffer[count++] = 0; transmitBuffer[count++] = dummy++;
	if(!(PIND & (1 << PIND0)) & (ch4 == 0)) //If button is being pressed, increment ch4
	{
		ch4 = 1;
	}
	if(ch4 == 1) //True for every second value of ch4
	{
		transmitBuffer[count++] = 2;
		transmitBuffer[count++] = (uint8_t)(bigdummy >> 8)& 0x3; transmitBuffer[count++] = (uint8_t)bigdummy--;
	}
	
	transmitBuffer[count++] = 'A'; //Node ID again
	
	SENDPACKETVAR(transmitBuffer,count); //Transmit data to receiver node
}

int main(void)
{
	setupUART();
	setupSPI();
	setupNRF();
	setupADC();
	setupButton();
	DDRD |= (1 << 5); //Set LED pin as output
	FLUSHTX();
	FLUSHRX();
	while(1)
	{
		sendAll(); //Build packet and load into TX FIFO
		CE_HIGH();
		
		while(!(WriteByteToNRF(R_REGISTER,STATUS,NOP) & (1 << TX_DS_bp))) //Continually polls STATUS until data sent
		{	
			if(WriteByteToNRF(R_REGISTER,STATUS,NOP) & (1 << MAX_RT_bp))
			{	
				PORTD |= (1 << 5); //Switch off LED to indicate transmission error
				WriteByteToNRF(W_REGISTER,STATUS,(1 << MAX_RT_bp));
				_delay_ms(1000); //Wait 1S before retrying
			}
		}
		FLUSHTX();
		PORTD &= ~(1 << 5); //Switch LED back on
		CE_LOW();
		WriteBytesToNRF(0, R_RX_PAYLOAD,NULL,WriteByteToNRF(R_REGISTER,R_RX_PL_WID,NOP)); //Get response (if any)
		WriteByteToNRF(W_REGISTER,STATUS,(1 << RX_DR_bp) | (1 << TX_DS_bp) | (1 << MAX_RT_bp)); //Clear interrupt flags
		FLUSHRX();
		_delay_ms(50);
	}
	
}