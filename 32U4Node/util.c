/*
  Michael Rodger 30 October 2014
  Some basic utilities
 */ 

uint8_t SPIRW(uint8_t data)
{
	SPDR = data;
	while(!(SPSR & (1 << SPIF)));
	return SPDR;
}

#ifdef __AVR_ATmega328P__
void UARTTX(uint8_t data)
{
	UDR0 = data;
	while(!(UCSR0A & (1 << TXC0))); //Wait for transmission to complete
	UCSR0A |= (1 << TXC0); //Clear flag
}
#endif

#ifdef __AVR_ATmega32U4__
void UARTTX(uint8_t data)
{
	UDR1 = data;
	while(!(UCSR1A & (1 << TXC1))); //Wait for transmission to complete
	UCSR1A |= (1 << TXC1); //Clear flag
}
#endif

void UARTPrintString(char data[])
{
	int count = 0;
	while(data[count] != '\0')
	{
		UARTTX(data[count++]);
		if(count > STRINGBUFFER) break; //failsafe
	}
}